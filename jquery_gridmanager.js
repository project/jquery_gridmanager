/**
 * @file
 * Javascript for jQuery Gridmanager.
 */

(function ($) {
  "use strict";

  /**
   * Instantiate Gridmanager on a node form.
   */
  Drupal.behaviors.jqueryGridmanager = {
    attach: function (context, settings) {
      var gm = $("#jquery_gridmanager-canvas").gridmanager({
        // Overridden because Drupal will add required assets.
        cssInclude: "",
        // Control bar RH dropdown markup. This is overridden to remove "Save"
        // button and enable translation.
        controlAppend: "<div class='btn-group pull-right'><button title='" + Drupal.t("Edit Source Code") + "' type='button' class='btn btn-xs btn-primary gm-edit-mode'><span class='fa fa-code'></span></button><button title='" + Drupal.t("Preview") + "' type='button' class='btn btn-xs btn-primary gm-preview'><span class='fa fa-eye'></span></button>     <div class='dropdown pull-left gm-layout-mode'><button type='button' class='btn btn-xs btn-primary dropdown-toggle' data-toggle='dropdown'><span class='caret'></span></button> <ul class='dropdown-menu' role='menu'><li><a data-width='auto' title='" + Drupal.t("Desktop") + "'><span class='fa fa-desktop'></span> " + Drupal.t("Desktop") + "</a></li><li><a title='" + Drupal.t("Tablet") + "' data-width='768'><span class='fa fa-tablet'></span> " + Drupal.t("Tablet") + "</a></li><li><a title='" + Drupal.t("Phone") + "' data-width='640'><span class='fa fa-mobile-phone'></span> " + Drupal.t("Phone") + "</a></li></ul></div>    <button type='button' class='btn  btn-xs  btn-primary dropdown-toggle' data-toggle='dropdown'><span class='caret'></span><span class='sr-only'>" + Drupal.t("Toggle Dropdown") + "</span></button><ul class='dropdown-menu' role='menu'><li><a title='" + Drupal.t("Reset Grid") + "' href='#' class='gm-resetgrid'><span class='fa fa-trash-o'></span> " + Drupal.t("Reset") + "</a></li></ul></div>"
      }).data('gridmanager');

      $("#edit-submit, #edit-preview").click(function () {
        gm.deinitCanvas();
        var $canvas = $('#jquery_gridmanager-canvas #gm-canvas');
        $('#jquery_gridmanager-canvas').siblings("input").val($canvas.html());
      });
    }
  };
})(jQuery);
