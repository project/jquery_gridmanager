core = 7.x
api = 2

libraries[bootstrap][type] = "libraries"
libraries[bootstrap][download][type] = "get"
libraries[bootstrap][download][url] = "https://github.com/twbs/bootstrap/archive/v3.1.1.zip"
libraries[bootstrap][directory_name] = "bootstrap-v3.1.1"
libraries[bootstrap][overwrite] = TRUE

libraries[font_awesome][type] = "libraries"
libraries[font_awesome][download][type] = "get"
libraries[font_awesome][download][url] = "https://github.com/FortAwesome/Font-Awesome/archive/v4.1.0.zip"
libraries[font_awesome][directory_name] = "font_awesome-v4.1.0"
libraries[font_awesome][overwrite] = TRUE

libraries[gridmanager][type] = "libraries"
libraries[gridmanager][download][type] = "get"
libraries[gridmanager][download][url] = "https://github.com/neokoenig/jQuery-gridmanager/archive/0.3.1.zip"
libraries[gridmanager][directory_name] = "gridmanager-0.3.1"
libraries[gridmanager][overwrite] = TRUE
