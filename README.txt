INTRODUCTION
============

Gridmanager allows you to create, reorder, update and delete rows and columns
in grid layouts on the fly.

To make use of this module add Gridmanager field to a Content type.
If default and admin themes are Bootstrap-based, uncheck checkbox on the config
page admin/config/content/jquery-gridmanager.


LIBRARIES
=========

Module relies on the following external libraries:

Bootstrap v3.1.1
Font-Awesome v4.1.0
jQuery-gridmanager 0.3.1


INSTALLATION
============

First download the listed libraries into your libraries folder by running:

"drush make --no-core jquery_gridmanager.make"

Then proceed with module installation as usual.
