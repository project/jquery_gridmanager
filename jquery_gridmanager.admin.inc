<?php

/**
 * @file
 * Administration page callbacks for the jQuery Gridmanager module.
 */

/**
 * Form constructor for the jquery_gridmanager settings form.
 */
function jquery_gridmanager_admin_settings() {
  $form['jquery_gridmanager_include_tw_bootstrap'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Bootstrap libraries to the Gridmanager pages'),
    '#default_value' => variable_get('jquery_gridmanager_include_tw_bootstrap', 1),
    '#description' => t('Disable this option if you have Bootstrap based theme.'),
  );
  $form['jquery_gridmanager_ckeditor_path'] = array(
    '#type' => 'textfield',
    '#title' => t('The path of the primary CKEditor file, relative to the Drupal root; e.g. sites/all/libraries/ckeditor/ckeditor.js.'),
    '#default_value' => variable_get('jquery_gridmanager_ckeditor_path', ''),
  );
  $form['jquery_gridmanager_ckeditor_adapter_path'] = array(
    '#type' => 'textfield',
    '#title' => t('The path of the CKEditor jQuery adapter file, relative to the Drupal root.'),
    '#default_value' => variable_get('jquery_gridmanager_ckeditor_adapter_path', ''),
  );

  return system_settings_form($form);
}
